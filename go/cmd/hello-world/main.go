package main

import (
	"fmt"

	// Example import, just to see if govendor is working for us
	_ "gitlab.com/gitlab-org/gitaly-proto/go"
)

func main() {
	fmt.Println("Hello, world!")
}
